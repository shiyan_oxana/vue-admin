<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'Admin']);
        $permission = Permission::create(['name' => 'login to dashboard']);
        $role->givePermissionTo($permission);
        $role = Role::create(['name' => 'User']);
        $permission = Permission::create(['name' => 'login to web']);
        $role->givePermissionTo($permission);
    }
}
