<?php

namespace Database\Seeders;

use App\Domain\Balance\Balance;
use App\Domain\User\Account;
use App\Domain\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

/**
 * Class UserSeeder
 * @package Database\Seeders
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'test@test.com';
        $user->password = bcrypt('testpass');
        $user->email_verified_at = Carbon::now();
        $user->assignRole('User');
        $user->save();
        $account = new Account();
        $account->full_name = 'User Name';
        $user->account()->save($account);

        $user = new User();
        $user->email = 'admin@test.com';
        $user->password = bcrypt('ntgkjdjp');
        $user->email_verified_at = Carbon::now();
        $user->assignRole('Admin');
        $user->save();
        $account = new Account();
        $account->full_name = 'Admin Name';
        $user->account()->save($account);
    }
}
