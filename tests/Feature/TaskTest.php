<?php

namespace Tests\Feature;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Task;
use Tests\TestCase;

class TaskTest extends TestCase
{
    /** @var User */
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /** @test */
    public function create_task()
    {
        $this->actingAs($this->user)
            ->postJson(route('tasks.store'), [
                'name' => 'Lorem'
            ])
            ->assertSuccessful()
            ->assertJson(['type' => Controller::RESPONSE_TYPE_SUCCESS]);

        $this->assertDatabaseHas('tasks', [
            'name' => 'Lorem',
        ]);
    }

    /** @test */
    public function update_task()
    {
        $task = Task::factory()->create();

        $this->actingAs($this->user)
            ->putJson(route('tasks.update', $task->id), [
                'name' => 'Updated task',
            ])
            ->assertSuccessful()
            ->assertJson(['type' => Controller::RESPONSE_TYPE_SUCCESS]);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'name' => 'Updated task',
        ]);
    }

    /** @test */
    public function show_task()
    {
        $task = Task::factory()->create();

        $this->actingAs($this->user)
            ->getJson(route('tasks.show', $task->id))
            ->assertSuccessful()
            ->assertJson([
                'data' => [
                    'name' => $task->name,
                ]
            ]);
    }

    /** @test */
    public function list_task()
    {
        $tasks = Task::factory()->count(2)->create()->map(function ($task) {
            return $task->only(['id', 'name']);
        });

        $this->actingAs($this->user)
            ->getJson(route('tasks.index'))
            ->assertSuccessful()
            ->assertJson([
                 'data' => $tasks->toArray()
            ])
            ->assertJsonStructure([
                'data' => [
                    '*' => ['id', 'name']
                ],
                'links',
                'meta'
            ]);
    }

    /** @test */
    public function delete_task()
    {
        $task = Task::factory()->create([
            'name' => 'Task for delete',
        ]);

        $this->actingAs($this->user)
            ->deleteJson(route('tasks.update', $task->id))
            ->assertSuccessful()
            ->assertJson(['type' => Controller::RESPONSE_TYPE_SUCCESS]);

        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
            'name' => 'Task for delete',
        ]);
    }
}
