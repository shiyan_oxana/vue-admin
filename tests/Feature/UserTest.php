<?php

declare(strict_types=1);

use Illuminate\Http\Response;
use Tests\Feature\TestCase as BaseTestCase;

/**
 * Class UserTest
 * @package Tests\Feature
 */
class UserTest extends BaseTestCase
{
    /**
     * @test
     */
    public function shouldIndexUser()
    {
        $this->signIn();

        $this
            ->json('get', route('api.users.index'), [], $this->headers())
            ->assertJson([
                'data' => [
                    ['type' => 'user']
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    [
                        'type',
                        'id',
                        'attributes' => [
                            'createdAt',
                            'updatedAt',
                        ],
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems',
                ],
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldFilterIndexUser()
    {
        $this->signIn();
        $data = ['filter' => ['query' => '']];

        $this
            ->json('get', route('api.users.index'), $data, $this->headers())
            ->assertJson([
                'data' => [
                ]
            ])
            ->assertJsonMissing([
                'data' => [
                ]
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldShowUser()
    {
        $this->signIn();
        $show = 1;

        $this
            ->json('get', route('api.users.show', $show), [], $this->headers())
            ->assertJson([
                'data' => [
                    'type' => 'user',
                    'id' => $show,
                ]
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldStoreUser()
    {
        $this->signIn();
        $data = [];

        $this
            ->json('post', route('api.users.store'), $data, $this->headers())
            ->assertJson(['data' => []])
            ->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('users', [
        ]);
    }

    /**
     * @test
     */
    public function shouldErrorStoreUser()
    {
        $this->signIn();

        $this
            ->json('post', route('api.users.store'), [], $this->headers())
            ->assertJson([
                'errors' => [
                ]
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function shouldUpdateUser()
    {
        $this->signIn();
        $update = 1;
        $data = [
        ];

        $this
            ->json('put', route('api.users.update', $update), $data, $this->headers())
            ->assertJson(['data' => []])
            ->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseHas('users', [
            'id' => $update
        ]);
    }

    /**
     * @test
     */
    public function shouldErrorUpdateUser()
    {
        $this->signIn();
        $update = 1;

        $this
            ->json('put', route('api.users.update', $update), [], $this->headers())
            ->assertJson([
                'errors' => [
                ]
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function shouldDeleteUser()
    {
        $this->signIn();
        $delete = 1;

        $this
            ->json('delete', route('api.users.destroy', $delete), [], $this->headers())
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('users', ['id' => $delete]);
    }

    /**
     * @test
     */
    public function shouldErrorsDeleteUser()
    {
        $this->signIn();
        $delete = 999;

        $this
            ->json('delete', route('api.users.destroy', $delete), [], $this->headers())
            ->assertJson([
                'errors' => [
                    $this->errorPageNotFound()
                ]
            ])
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
