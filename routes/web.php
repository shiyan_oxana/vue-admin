<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'as' => 'admin.'
], function () {
    Route::view('/', 'admin')->where('any', '.*')->name('login');
    Route::view('/admin/', 'admin')->where('any', '.*')->name('index');
    Route::view('/admin/{any}', 'admin')->where('any', '.*')->name('any');
});
