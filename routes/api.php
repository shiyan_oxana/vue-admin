<?php

use App\Api\V1\Http\Controllers\AuthController;
use App\Api\V1\Http\Controllers\LogController;
use App\Api\V1\Http\Controllers\TaskController;
use App\Api\V1\Http\Controllers\UserController;
use App\Domain\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'as' => 'api.v1.'
], function () {
    Route::prefix('v1')->group(function () {
        Route::prefix('auth')->group(function () {
            Route::post('login', AuthController::class . '@login')
                ->name('login');

            Route::middleware('auth:api')->group(function () {
                Route::post('logout', AuthController::class . '@logout')
                    ->name('logout');
            });
        });

        Route::middleware('auth:api')->group(function () {
            Route::prefix('log')->group(function () {
                Route::middleware('apiRole:' . User::ADMIN_ROLE_NAME)->group(function () {
                    Route::get('/', LogController::class . '@index')
                        ->name('index');
                    Route::get('/download/{key}', LogController::class . '@download')
                        ->name('download');
                    Route::post('/clear/{key}', LogController::class . '@clear')
                        ->name('clear');
                    Route::get('/{key}', LogController::class . '@show')
                        ->name('show');
                });
            });
        });

        Route::middleware('auth:api')->group(function () {
            Route::prefix('user')->group(function () {
                Route::get('me', UserController::class . '@me')->name('me');
                Route::get('/', UserController::class . '@index')->name('index');
                Route::get('{user}', UserController::class . '@show')->name('show')->where('user', '[0-9]+');
            });
        });

        Route::name('settings.')
            ->group(app_path('Api/V1/Http/Routes/SettingRoutes.php'));
        Route::name('tasks.')
            ->group(app_path('Api/V1/Http/Routes/TaskRoutes.php'));
    });
});
