<?php
namespace App\Helpers;

use App\Infrastructure\Settings\SystemSettingsManager;

class SystemHelper
{
    /**
     * @param null $settingsManager
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function getUiUrl($settingsManager = null)
    {
        if (!$settingsManager) {
            $settingsManager = app()->make(SystemSettingsManager::class);
        }

        return $settingsManager->get('system.ui_url')
            ? $settingsManager->get('system.ui_url')
            : config('app.url');
    }
}
