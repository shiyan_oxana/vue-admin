<?php
namespace App\Helpers;

class PaginationHelper
{
    /**
     * @param int $page
     * @param int $perPage
     * @param string $routeName
     * @param int $totalItems
     * @return array
     */
    public function links($page, $perPage, $routeName, $totalItems)
    {
        $request = request()->route()->parameters();

        $totalPages = $this->totalPages($totalItems, $perPage);

        $links = [
            'self' => route($routeName, array_merge($request, [
                'page' => $page,
                'perPage' => $perPage
            ])),
            'first' => route($routeName, array_merge($request, [
                'page' => 1,
                'perPage' => $perPage
            ])),
            'last' => route($routeName, array_merge($request, [
                'page' => $totalPages == 0  ? 1 : $totalPages,
                'perPage' => $perPage
            ]))
        ];

        if ($page > 1) {
            $links['prev'] = route($routeName, array_merge($request, [
                'page' => $page - 1,
                'perPage' => $perPage
            ]));
        }
        if ($page != $totalPages && $totalItems != 0) {
            $links['next'] = route($routeName, array_merge($request, [
                'page' => $page + 1,
                'perPage' => $perPage
            ]));
        }
        return $links;
    }

    /**
     * @param int $perPage
     * @param int $totalItems
     * @return float
     */
    public function totalPages($totalItems, $perPage)
    {
        if (!$perPage) {
            return 1;
        }
        return (int)ceil($totalItems / $perPage);
    }
}
