<?php
namespace App\Helpers;

use Exception;

/**
 * Class LinkHelper
 * @package App\Helpers
 */
class LinkHelper
{
    /**
     * @type string
     * part of path www
     */
    const WWW = 'www.';

    /**
     * @param $url
     * @return mixed
     */
    public static function trimWWW($url)
    {
        if (strpos($url, self::WWW) === 0) {
            $url = substr_replace($url, '', 0, strlen(self::WWW));
        }

        return $url;
    }

    /**
     * @param string $url
     * @return array
     */
    public static function getUrlWithWWW($url)
    {
        $urls[] = self::trimWWW($url);
        $urls[] = self::WWW . self::trimWWW($url);

        return $urls;
    }

    /**
     * @param $url
     * @return string
     */
    public static function trimProtocol($url)
    {
        $url = preg_replace("(^//)", '', preg_replace("/^https?:/i", '', $url));

        return $url;
    }
    /**
     * @param $url
     * @return string|bool
     */
    public static function getProtocol($url)
    {
        if ($url == 'http' || $url == 'https') {
            return $url;
        }

        if (strpos($url, 'http') === 0 || strpos($url, 'https')) {
            $url = parse_url($url);
            return array_key_exists('scheme', $url) ? $url['scheme'] : false;
        }

        return false;
    }

    /**
     * @param string $url
     * @param bool $like
     * @return array
     */
    public static function getUrls($url, $like = false)
    {
        $symbol = $like ? '%' : '';

        $urls[] =self::trimWWW($url);
        $urls[] = self::WWW . self::trimWWW($url);

        foreach ($urls as $key => $u) {
            if (substr($u, -1) == '/') {
                $urls[] = $symbol . substr($u, 0, -1) . $symbol;
            } else {
                $urls[] = $symbol . $u . '/' . $symbol;
            }

            $urls[$key] = $symbol . $u . $symbol;
        }

        return $urls;
    }
}
