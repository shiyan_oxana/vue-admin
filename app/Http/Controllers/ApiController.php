<?php

namespace App\Http\Controllers;

use App\Domain\Settings\SettingsManagerInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\CommandBus;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var CommandBus
     */
    private $dispatcher;
    /**
     * @var AuthManager
     */
    protected $authManager;
    /**
     * @var SettingsManagerInterface
     */
    protected $settingsManager;
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * ApiController constructor.
     * @param CommandBus $dispatcher
     * @param AuthManager $authManager
     * @param SettingsManagerInterface $settingsManager
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        CommandBus $dispatcher,
        AuthManager $authManager,
        SettingsManagerInterface $settingsManager,
        UserRepositoryInterface $userRepository
    ) {
        $this->dispatcher = $dispatcher;
        $this->authManager = $authManager;
        $this->settingsManager = $settingsManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Command $command
     * @return mixed
     */
    public function dispatch(Command $command)
    {
        $result = $this->dispatcher->execute($command);

        return $result;
    }

    /**
     * @return User|Authenticatable|null
     */
    protected function user(): ?Authenticatable
    {
        return $this->authManager->guard('api')->user();
    }
}
