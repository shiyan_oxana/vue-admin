<?php

namespace App\Providers;


use App\Domain\Task\TaskRepositoryInterface;
use App\Domain\User\UserRepositoryInterface;
use App\Infrastructure\Eloquent\EloquentTaskRepository;
use App\Infrastructure\Eloquent\EloquentUserRepository;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Infrastructure\Eloquent\EloquentSettingsRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class DomainServiceProvider
 * @package App\Providers
 */
class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SettingsRepositoryInterface::class, EloquentSettingsRepository::class);
        $this->app->singleton(UserRepositoryInterface::class, EloquentUserRepository::class);
        $this->app->singleton(TaskRepositoryInterface::class, EloquentTaskRepository::class);
    }

    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        //
    }
}
