<?php

namespace App\Providers;

use App\Domain\Settings\SettingsManagerInterface;
use App\Infrastructure\Settings\SystemSettingsManager;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SettingsManagerInterface::class, SystemSettingsManager::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
