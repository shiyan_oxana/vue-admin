<?php

namespace App\Api\V1\Http\Requests\Task;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TaskStoreRequest
 * @package App\Api\V1\Http\Requests\Task
 */
class TaskStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:191'
        ];
    }
}
