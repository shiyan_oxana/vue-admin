<?php

namespace App\Api\V1\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UserUploadAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'required|image|max:5120',
        ];
    }
}
