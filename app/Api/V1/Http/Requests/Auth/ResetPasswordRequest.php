<?php

namespace App\Api\V1\Http\Requests\Auth;

use App\Domain\Settings\SettingsManagerInterface;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * @var SettingsManagerInterface
     */
    private $settingsManager;

    /**
     * ResetPasswordRequest constructor.
     * @param SettingsManagerInterface $settingsManager
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     */
    public function __construct(
        SettingsManagerInterface $settingsManager,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->settingsManager = $settingsManager;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $min = $this->settingsManager->get('site.password_min_limit');
        return [
            'password' => "required|string|min:{$min}|max:30",
            'passwordConfirmation' => 'required|same:password',
            'code' => 'required|string|exists:users,reset_password_token'
        ];
    }

    public function messages()
    {
        return [
            'code.exists' => trans('validation.code.incorrect'),
        ];
    }
}
