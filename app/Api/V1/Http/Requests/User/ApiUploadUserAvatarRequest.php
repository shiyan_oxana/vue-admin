<?php

namespace App\Api\V1\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ApiUploadUserAvatarRequest
 * @package App\Api\V1\Http\Requests\User
 */
class ApiUploadUserAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'required|image',
        ];
    }
}
