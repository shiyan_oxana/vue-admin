<?php

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Application\Task\GetTaskList\GetTaskList;
use App\Api\V1\Application\Task\CreateTask\CreateTask;
use App\Api\V1\Application\Task\UpdateTask\UpdateTask;
use App\Api\V1\Application\Task\DeleteTask\DeleteTask;
use App\Api\V1\Http\Requests\Task\TaskIndexRequest;
use App\Api\V1\Http\Requests\Task\TaskStoreRequest;
use App\Api\V1\Http\Requests\Task\TaskUpdateRequest;
use App\Api\V1\Http\Resources\Task\TaskResource;
use App\Api\V1\Http\Resources\Task\TaskResourceCollection;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use App\Domain\Settings\SettingNotFound;
use App\Domain\Task\Task;
use App\Domain\Task\TaskFilter;
use App\Http\Controllers\ApiController;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class TaskController
 * @package app/Api/V1/Http\Controllers
 */
class TaskController extends ApiController
{
    /**
     * @param TaskIndexRequest $request
     * @return JsonResponse
     * @throws BindingResolutionException
     * @throws SettingNotFound
     */
    public function index(TaskIndexRequest $request): JsonResponse
    {
        $taskFilter = TaskFilter::fromRequest($request);

        $sort = Sorting::fromRequest($request, Task::ALLOWED_SORT_FIELDS);
        $pagination = Pagination::fromRequest($request);

        $tasks = $this->dispatch(new GetTaskList($taskFilter, $pagination, $sort));

        return response()->json(new TaskResourceCollection($tasks), Response::HTTP_OK);
    }

    /**
     * @param Task $task
     * @return JsonResponse
     */
    public function show(Task $task)
    {
        return response()->json([
            'data' => new TaskResource($task)
        ], Response::HTTP_OK);
    }

    /**
     * @param Task $task
     * @param TaskUpdateRequest $request
     * @return JsonResponse
     */
    public function update(Task $task, TaskUpdateRequest $request)
    {
        $task = $this->dispatch(
            new UpdateTask(
                $task,
                $request->get('name')
            )
        );

        return response()->json([
            'data' => new TaskResource($task)
        ], Response::HTTP_OK);
    }

    public function store(TaskStoreRequest $request)
    {
        $task = $this->dispatch(
            new CreateTask(
                $request->get('name')
            )
        );

        return response()->json([
            'data' => new TaskResource($task)
        ], Response::HTTP_OK);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return JsonResponse
     */
    public function destroy(Task $task): JsonResponse
    {
        $this->dispatch(new DeleteTask($task->id));

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
