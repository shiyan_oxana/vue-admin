<?php

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Application\User\DeleteUser\DeleteUser;
use App\Api\V1\Application\User\GetUserList\GetUserList;
use App\Api\V1\Http\Requests\User\UserIndexRequest;
use App\Api\V1\Http\Resources\User\MeResource;
use App\Api\V1\Http\Resources\User\UserResource;
use App\Api\V1\Http\Resources\User\UserResourceCollection;
use App\Domain\Core\Sorting;
use App\Domain\Core\Pagination;
use App\Domain\Settings\SettingNotFound;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Http\Controllers\ApiController;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class UserController
 * @package App\Api\V1\Http\Controllers
 */
class UserController extends ApiController
{
    /**
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
        $me = $this->authManager->guard('api')->user();

        return \response()->json([
            'data' => new MeResource($me)
        ]);
    }

    /**
     * @param UserIndexRequest $request
     * @return JsonResponse
     * @throws SettingNotFound
     * @throws BindingResolutionException
     */
    public function index(UserIndexRequest $request): JsonResponse
    {
        $userFilter = UserFilter::fromRequest($request);
        $sort = Sorting::fromRequest($request, User::ALLOWED_SORT_FIELDS);
        $pagination = Pagination::fromRequest($request);

        $users = $this->dispatch(new GetUserList($userFilter, $pagination, $sort));

        return response()->json(new UserResourceCollection($users), Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @return JsonResponse
     * @throws
     */
    public function show(User $user): JsonResponse
    {
        return response()->json(['data' => new UserResource($user)]);
    }


    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->dispatch(new DeleteUser($id));

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
