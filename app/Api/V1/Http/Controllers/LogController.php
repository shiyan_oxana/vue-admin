<?php

namespace App\Api\V1\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;

/**
 * Class LogController
 * @package App\Api\V1\Http\Controllers
 */
class LogController extends BaseController
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * @var LaravelLogViewer
     */
    private $log_viewer;

    /**
     * @var string
     */
    protected $view_log = 'laravel-log-viewer::log';

    /**
     * LogViewerController constructor.
     */
    public function __construct()
    {
        $this->log_viewer = new LaravelLogViewer();
        $this->request = app('request');
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $files = $this->log_viewer->getFiles(true);

        foreach ($files as $key => $value) {
            $files[$key] = [
                'name' => $value,
                'key' => Crypt::encrypt($value)
            ];
        }


        $data = [
            'files' => $files
        ];

        return response()->json(new JsonResource($data), Response::HTTP_OK);
    }

    /**
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function download($key)
    {
        $this->log_viewer->pathToLogFile(Crypt::decrypt($key));

        return response()->download($this->log_viewer->pathToLogFile(Crypt::decrypt($key)));
    }

    public function clear($key)
    {
        app('files')->put($this->log_viewer->pathToLogFile(Crypt::decrypt($key)), '');

        return response()->json(new JsonResource([]), Response::HTTP_OK);
    }
}
