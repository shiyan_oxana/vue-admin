<?php

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Application\Auth\Login;
use App\Api\V1\Application\Auth\Logout;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class AuthController
 * @package App\Api\V1\Auth\Http\Controllers
 */
class AuthController extends ApiController
{
    /**
     * @param ServerRequestInterface $request
     * @return JsonResponse
     */
    public function login(ServerRequestInterface $request): JsonResponse
    {
            try {
                $validator = Validator::make($request->getParsedBody(), [
                    'email' => 'required|string',
                    'password' => 'required|string'
                ]);
                $validator->validate();
            } catch (ValidationException $ex) {
                return \response()->json([
                    'message' => __('validation.validation_failed'),
                    'errors' => $ex->validator->errors()->messages()
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $token = $this->dispatch(new Login($request));
            if (isset($token['error'])) {
                return \response()->json([
                    'errors' => [
                        [
                            'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                            'detail' => $token['message'],
                        ]
                    ]
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return \response()->json([
                'data' => [
                    'type' => 'token',
                    'attributes' => [
                        'tokenType' => $token['token_type'],
                        'expiresIn' => $token['expires_in'],
                        'accessToken' => $token['access_token'],
                        'refreshToken' => $token['refresh_token'],
                    ],
                ]
            ]);

    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        $this->dispatch(new Logout());
        return \response()->json('', Response::HTTP_NO_CONTENT);
    }
}
