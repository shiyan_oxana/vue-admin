<?php

namespace App\Api\V1\Http\Controllers;

use App\Api\V1\Http\Resources\Setting\SettingListResource;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class SettingController
 * @package App\Api\V1\Http\Controllers
 */
class SettingController extends ApiController
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return \response()->json(new SettingListResource($this->settingsManager->all()), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        $response = ['success' => true];

        if ($request->get('validationRules')) {
            $this->validate($request, [
                'itemValue' => $request->get('validationRules')
            ]);
        }

        $key = $request->get('group') . '.' . $request->get('itemKey');
        $this->settingsManager->set($key, $request->get('itemValue'));

        return response()->json($response);
    }
}
