<?php

namespace App\Api\V1\Http\Middleware;

use App\Exceptions\ForbiddenException;
use Closure;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

/**
 * Class ApiCheckRole
 * @package Api\Http\Middleware
 */
class ApiCheckRole
{

    /**
     * @var AuthManager
     */
    private $auth;

    /**
     * @param AuthManager $auth
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param $request
     * @param Closure $next
     * @param string $role
     * @return RedirectResponse|Redirector|mixed
     * @throws ForbiddenException
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $this->auth->guard('api')->user();

        if (!$user->hasRole($role)) {
            throw new ForbiddenException();
        }

        return $next($request);
    }
}
