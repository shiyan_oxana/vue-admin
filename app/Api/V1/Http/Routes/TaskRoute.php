<?php

use App\Api\V1\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api']], function () {
     Route::prefix('tasks')->group(function () {
        Route::get('/', TaskController::class . '@index')->name('index');
        Route::get('/{task}', TaskController::class . '@show')->name('show');
        Route::put('/{task}', TaskController::class . '@update')->name('update');
        Route::post('/', TaskController::class . '@store')->name('store');
        Route::delete('/{task}', TaskController::class . '@destroy')->name('destroy');
     });
});
