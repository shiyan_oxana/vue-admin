<?php

use App\Domain\User\User;
use Illuminate\Support\Facades\Route;
use App\Api\V1\Http\Controllers\SettingController;

Route::group(['middleware' => ['auth:api', 'apiRole:' . User::ADMIN_ROLE_NAME]], function () {
    Route::prefix('settings')->group(function () {
        Route::get('/', SettingController::class . '@index')
            ->name('index');
        Route::post('/update', SettingController::class . '@update')
            ->name('update');
    });
});
