<?php

namespace App\Api\V1\Http\Resources\Setting;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\App;
use ReflectionMethod;

/**
 * Class StateListResource
 * @package App\Api\V1\Http\Resources\State
 */
class SettingListResource extends ResourceCollection
{

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];
        foreach ($this->collection as $item) {
            $items = [];

            foreach ($item->all() as $key => $value) {
                $items[] = [
                    'itemKey' => $value->itemKey(),
                    'itemValue' => $value->itemValue(),
                    'label' => $value->label(),
                    'description' => $value->description(),
                    'group' => $item->key(),
                    'validationRules' => $value->validationRules(),
                    'options' => $value->options ? $this->getSettingOptions($item, $value->options) : null
                ];
            }

            $response[] = [
                'type' => 'setting',
                'attributes' => [
                    'key' => $item->key(),
                    'label' => $item->label(),
                    'items' => $items,
                ]
            ];
        }
        return ['data' => $response];
    }

    /**
     * @param $container
     * @param $optionsMethod
     * @return mixed
     */
    private function getSettingOptions($container, $optionsMethod)
    {
        return App::call([$container, $optionsMethod]);
    }
}
