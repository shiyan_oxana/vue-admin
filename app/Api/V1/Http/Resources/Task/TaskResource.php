<?php

namespace App\Api\V1\Http\Resources\Task;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TaskResource
 * @package App\Api\V1\Http\Resources\Task
 */
class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $response = [
            'type' => 'task',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name
            ],
        ];

        return $response;
    }
}
