<?php

namespace App\Api\V1\Http\Resources\Task;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CreateTaskResource
 * @package App\Api\V1\Http\Resources\Task
 */
class CreateTaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $response = [
            'type' => 'task'
        ];

        return ['data' => $response];
    }
}
