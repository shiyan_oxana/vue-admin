<?php

namespace App\Api\V1\Http\Resources\Task;

use App\Api\V1\Http\Resources\ResourceCollectionWithStandardResponse;

/**
 * Class TaskResourceCollection
 * @package App\Api\V1\Http\Resources\Task
 */
class TaskResourceCollection extends ResourceCollectionWithStandardResponse
{
    /**
     * @param object $item
     * @return TaskResource
     */
    protected function getItemData($item): TaskResource
    {
        return new TaskResource($item);
    }

    /**
     * @return null|string
     */
    protected function getLinksHelperRouteName()
    {
        return null;
    }
}
