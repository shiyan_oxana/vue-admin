<?php

namespace App\Api\V1\Http\Resources\User;

use App\Domain\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Api\V1\Http\Resources\User
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $response = [
            'type' => 'user',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->account ? $this->account->full_name : null,
                'email' => $this->email,
                'apiKey' => $this->account ? $this->account->api_key : '',
                'role' => $this->hasRole(User::ADMIN_ROLE_NAME) ? User::ADMIN_ROLE_NAME : User::USER_ROLE_NAME,
                'emailVerified' => $this->email_verified_at ?? false,
                'emailVerificationToken' => $this->email_verification_token,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at,
            ],
        ];

        return $response;
    }
}
