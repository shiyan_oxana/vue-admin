<?php

namespace App\Api\V1\Http\Resources\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CreateUserResource
 * @package App\Api\V1\Http\Resources\User
 */
class CreateUserResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [
            'type' => 'user',
        ];

        return ['data' => $response];
    }
}
