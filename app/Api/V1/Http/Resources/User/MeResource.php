<?php

namespace App\Api\V1\Http\Resources\User;

use App\Domain\User\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MeResource
 * @package App\Api\V1\Http\Resources\User
 */
class MeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     * @throws BindingResolutionException
     */
    public function toArray($request)
    {
        $response = [
            'type' => 'user',
            'attributes' => [
                'email' => $this->email,
                'name' => $this->account ? $this->account->full_name : null,
                'apiKey' => $this->account ? $this->account->api_key : null
            ],
        ];

        if ($this->hasRole(User::ADMIN_ROLE_ID)) {
            $response['attributes']['isAdmin'] = true;
        }

        return $response;
    }
}
