<?php

namespace App\Api\V1\Http\Resources\User;

use App\Api\V1\Http\Resources\ResourceCollectionWithStandardResponse;

/**
 * Class UserResourceCollection
 * @package App\Api\V1\Http\Resources\User
 */
class UserResourceCollection extends ResourceCollectionWithStandardResponse
{
    /**
     * @param object $item
     * @return UserResource
     */
    protected function getItemData($item): UserResource
    {
        return new UserResource($item);
    }

    /**
     * @return null|string
     */
    protected function getLinksHelperRouteName()
    {
        return null;
    }
}
