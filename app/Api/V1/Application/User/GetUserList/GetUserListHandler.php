<?php

namespace App\Api\V1\Application\User\GetUserList;

use App\Api\V1\Application\AbstractBus\AbstractGetListHandler;
use App\Domain\User\UserRepositoryInterface;
use ItDevgroup\CommandBus\Handler;

/**
 * Class GetUserListHandler
 * @package App\Api\V1\Application\User\GetUserList
 */
class GetUserListHandler extends AbstractGetListHandler implements Handler
{
    /**
     * GetBackendUserListHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->repository = $userRepository;
    }
}
