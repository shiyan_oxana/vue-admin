<?php

namespace App\Api\V1\Application\User\GetUserList;

use App\Api\V1\Application\AbstractBus\AbstractGetList;
use App\Domain\Core\Sorting;
use App\Domain\Core\Pagination;
use App\Domain\User\UserFilter;
use ItDevgroup\CommandBus\Command;

/**
 * Class GetUserList
 * @package App\Api\V1\Application\User\GetUserList
 */
class GetUserList extends AbstractGetList implements Command
{
}
