<?php

namespace App\Api\V1\Application\User\DeleteUser;

use App\Api\V1\Application\AbstractBus\AbstractDeleteCommand;

/**
 * Class DeleteUser
 * @package App\Api\V1\Application\User\DeleteUser
 */
class DeleteUser extends AbstractDeleteCommand
{

}
