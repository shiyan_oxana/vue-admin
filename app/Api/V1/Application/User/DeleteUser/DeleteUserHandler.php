<?php

namespace App\Api\V1\Application\User\DeleteUser;

use App\Api\V1\Application\AbstractBus\AbstractDeleteHandler;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class DeleteUserHandler
 * @package App\Api\V1\Application\User\DeleteUser
 */
class DeleteUserHandler extends AbstractDeleteHandler
{
    /**
     * UserDeleteHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->repository = $userRepository;
    }
}
