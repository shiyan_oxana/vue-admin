<?php

namespace App\Api\V1\Application\AbstractBus;

use App\Domain\Core\Filter;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use ItDevgroup\CommandBus\Command;

/**
 * Class AbstractGetList
 * @package App\Api\V1\Application\AbstractBus
 */
abstract class AbstractGetList implements Command
{
    /**
     * @var Filter|null
     */
    private ?Filter $filter;
    /**
     * @var Pagination|null
     */
    private ?Pagination $pagination;
    /**
     * @var Sorting|null
     */
    private ?Sorting $order;
    /**
     * @var array|null
     */
    private ?array $relations;

    /**
     * AbstractGetList constructor.
     * @param Filter|null $filter
     * @param Pagination|null $pagination
     * @param Sorting|null $order
     * @param array|null $relations
     */
    public function __construct(
        ?Filter $filter,
        ?Pagination $pagination = null,
        ?Sorting $order = null,
        ?array $relations = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->order = $order;
        $this->relations = $relations;
    }

    /**
     * @return Filter|null
     */
    public function getFilter(): ?Filter
    {
        return $this->filter;
    }

    /**
     * @return Pagination|null
     */
    public function getPagination(): ?Pagination
    {
        return $this->pagination;
    }

    /**
     * @return Sorting|null
     */
    public function getOrder(): ?Sorting
    {
        return $this->order;
    }

    /**
     * @return array|null
     */
    public function getRelations(): ?array
    {
        return $this->relations;
    }
}
