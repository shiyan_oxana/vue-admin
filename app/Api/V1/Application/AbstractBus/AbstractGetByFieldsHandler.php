<?php

namespace App\Api\V1\Application\AbstractBus;

use App\Infrastructure\Eloquent\AbstractEloquentRepository;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;

/**
 * Class AbstractGetByFieldsHandler
 * @package App\Api\V1\Application\AbstractBus
 */
class AbstractGetByFieldsHandler implements Handler
{
    /**
     * @var AbstractEloquentRepository
     */
    protected AbstractEloquentRepository $repository;

    /**
     * Handle a Command object
     *
     * @param Command|AbstractGetByFields $command
     * @return mixed
     */
    public function handle(Command $command)
    {
        return $this->repository->getFirstByFields(
            $command->getFields()
        );
    }
}
