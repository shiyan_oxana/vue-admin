<?php

namespace App\Api\V1\Application\AbstractBus;

use ItDevgroup\CommandBus\Command;

/**
 * Class AbstractGetById
 * @package App\Application\AbstractBus
 */
abstract class AbstractGetById implements Command
{
    /**
     * @var int
     */
    private int $id;

    /**
     * AbstractGetById constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
