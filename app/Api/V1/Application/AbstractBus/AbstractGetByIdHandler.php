<?php

namespace App\Api\V1\Application\AbstractBus;

use App\Infrastructure\Eloquent\AbstractEloquentRepository;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;

/**
 * Class AbstractGetByIdHandler
 * @package App\Application\AbstractBus
 */
class AbstractGetByIdHandler implements Handler
{
    /**
     * @var AbstractEloquentRepository
     */
    protected AbstractEloquentRepository $repository;

    /**
     * Handle a Command object
     *
     * @param Command|AbstractGetList $command
     * @return mixed
     */
    public function handle(Command $command)
    {
        return $this->repository->byId($command->id());
    }
}
