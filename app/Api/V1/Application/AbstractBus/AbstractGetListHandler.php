<?php

namespace App\Api\V1\Application\AbstractBus;

use App\Infrastructure\Eloquent\AbstractEloquentRepository;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;

/**
 * Class AbstractGetListHandler
 * @package App\Api\V1\Application\AbstractBus
 */
class AbstractGetListHandler implements Handler
{
    /**
     * @var AbstractEloquentRepository
     */
    protected AbstractEloquentRepository $repository;

    /**
     * Handle a Command object
     *
     * @param Command|AbstractGetList $command
     * @return mixed
     */
    public function handle(Command $command)
    {
        return $this->repository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getOrder(),
            $command->getRelations()
        );
    }
}
