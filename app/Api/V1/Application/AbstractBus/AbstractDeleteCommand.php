<?php

namespace App\Api\V1\Application\AbstractBus;

use ItDevgroup\CommandBus\Command;

/**
 * Class AbstractDeleteCommand
 * @package App\Application\AbstractBus
 */
abstract class AbstractDeleteCommand implements Command
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * AbstractDeleteCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
