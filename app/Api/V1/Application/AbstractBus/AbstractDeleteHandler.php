<?php

namespace App\Api\V1\Application\AbstractBus;

use App\Infrastructure\Eloquent\AbstractEloquentRepository;
use Exception;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;

/**
 * Class AbstractDeleteHandler
 * @package App\Application\AbstractBus
 */
abstract class AbstractDeleteHandler implements Handler
{
    /**
     * @var AbstractEloquentRepository
     */
    protected AbstractEloquentRepository $repository;

    /**
     * Handle a Command object
     *
     * @param Command|AbstractDeleteCommand $command
     * @return void
     * @throws Exception
     */
    public function handle(Command $command): void
    {
        $category = $this->repository->byId($command->id());

        $this->repository->delete($category);
    }
}
