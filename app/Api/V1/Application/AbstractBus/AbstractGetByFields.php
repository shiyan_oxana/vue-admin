<?php

namespace App\Api\V1\Application\AbstractBus;

use App\Domain\Core\Filter;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use ItDevgroup\CommandBus\Command;

/**
 * Class AbstractGetByFields
 * @package App\Api\V1\Application\AbstractBus
 */
abstract class AbstractGetByFields implements Command
{
    private array $fields;

    /**
     * AbstractGetByFields constructor.
     * @param array $fields
     */
    public function __construct(
        array $fields
    ) {
        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}
