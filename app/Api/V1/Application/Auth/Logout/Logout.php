<?php

namespace App\Api\V1\Application\Auth;

use ItDevgroup\CommandBus\Command;

/**
 * Class Logout
 * @package App\Api\V1\Application\Auth
 */
class Logout implements Command
{
}
