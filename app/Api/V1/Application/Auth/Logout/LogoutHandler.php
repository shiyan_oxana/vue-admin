<?php

namespace App\Api\V1\Application\Auth;

use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;
use Laravel\Passport\Token;

/**
 * Class LogoutHandler
 * @package App\Api\V1\Application\Auth
 */
class LogoutHandler implements Handler
{
    /**
     * Handle a Command object
     *
     * @param Command|Logout $command
     * @return mixed
     */
    public function handle(Command $command)
    {
        /** @var Token $token */
        $token = auth('api')->user()->token();

        if ($token) {
            $token->revoke();
        }
    }
}
