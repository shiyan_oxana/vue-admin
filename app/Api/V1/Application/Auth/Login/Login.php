<?php

namespace App\Api\V1\Application\Auth;

use ItDevgroup\CommandBus\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class Login
 * @package App\Api\V1\Application\Auth
 */
class Login implements Command
{
    /**
     * @var ServerRequestInterface
     */
    private $request;

    /**
     * Login constructor.
     * @param ServerRequestInterface $request
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @return ServerRequestInterface
     */
    public function request(): ServerRequestInterface
    {
        return $this->request;
    }
}
