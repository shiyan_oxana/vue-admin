<?php

namespace App\Api\V1\Application\Auth;

use App\Domain\User\UserRepositoryInterface;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;
use Laravel\Passport\Exceptions\OAuthServerException;
use Laravel\Passport\Http\Controllers\HandlesOAuthErrors;
use League\OAuth2\Server\AuthorizationServer;

/***
 * Class LoginHandler
 * @package App\Api\V1\Application\Auth
 */
class LoginHandler implements Handler
{
    use HandlesOAuthErrors;
    /**
     * @var AuthorizationServer
     */
    private $server;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * LoginHandler constructor.
     * @param AuthorizationServer $server
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        AuthorizationServer $server,
        UserRepositoryInterface $userRepository
    ) {
        $this->server = $server;
        $this->userRepository = $userRepository;
    }

    /**
     * Handle a Command object
     *
     * @param Command|Login $command
     * @return array
     * @throws ValidationException
     * @throws OAuthServerException
     */
    public function handle(Command $command): array
    {
        $array = $command->request()->getParsedBody();

        try {

            $user = $this->userRepository->getFirstByFields(['email' => $array['email']]);

            if (!Hash::check($array['password'], $user->password)) {
                throw new ModelNotFoundException();
            }

            if (Hash::check($array['password'], $user->password) && $user->email_verified_at == null) {
                throw ValidationException::withMessages([
                    'email' => 'Email not verified'
                ]);
            }
        } catch (ModelNotFoundException $e) {
            throw ValidationException::withMessages([
                'email' => trans('validation.credentials')
            ]);
        }

        $client = DB::table('oauth_clients')->where('password_client', true)
            ->first();

        $array['username'] = $array['email'];
        $array['client_id'] = $client->id;
        $array['client_secret'] = $client->secret;
        $array['scope'] = '*';
        $array['grant_type'] = 'password';
        $request = $command->request()->withParsedBody($array);

        $res = $this->withErrorHandling(function () use ($request) {
            return $this->convertResponse(
                $this->server->respondToAccessTokenRequest($request, new Response())
            );
        });

        $res = json_decode($res->getContent(), true);

        return $res;
    }
}
