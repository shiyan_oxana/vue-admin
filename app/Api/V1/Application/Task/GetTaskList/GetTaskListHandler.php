<?php

namespace App\Api\V1\Application\Task\GetTaskList;


use App\Api\V1\Application\AbstractBus\AbstractGetListHandler;
use App\Domain\Task\TaskRepositoryInterface;
use ItDevgroup\CommandBus\Handler;

/**
 * Class GetTaskListHandler
 * @package App\Api\V1\Application\Task\GetTaskList
 */
class GetTaskListHandler extends AbstractGetListHandler
{
    /**
     * GetTaskListHandler constructor.
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository
    ) {
        $this->repository = $taskRepository;
    }

}
