<?php

namespace App\Api\V1\Application\Task\GetTaskList;

use App\Api\V1\Application\AbstractBus\AbstractGetList;

/**
 * Class GetTaskList
 * @package App\Api\V1\Application\Task\GetTaskList
 */
class GetTaskList extends AbstractGetList
{
}
