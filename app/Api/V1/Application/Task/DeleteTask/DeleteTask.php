<?php

namespace App\Api\V1\Application\Task\DeleteTask;

use App\Api\V1\Application\AbstractBus\AbstractDeleteCommand;

/**
 * Class DeleteTask
 * @package App\Api\V1\Application\Task\DeleteTask
 */
class DeleteTask extends AbstractDeleteCommand
{

}
