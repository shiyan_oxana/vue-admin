<?php

namespace App\Api\V1\Application\Task\DeleteTask;

use App\Api\V1\Application\AbstractBus\AbstractDeleteHandler;
use App\Domain\Task\TaskRepositoryInterface;

/**
 * Class DeleteTaskHandler
 * @package App\Api\V1\Application\Task\DeleteTask
 */
class DeleteTaskHandler extends AbstractDeleteHandler
{
    /**
     * TaskDeleteHandler constructor.
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository
    ) {
        $this->repository = $taskRepository;
    }
}
