<?php

namespace App\Api\V1\Application\Task\UpdateTask;

use App\Domain\Core\ModelNotSavedException;
use App\Domain\Task\Task;
use App\Domain\Task\TaskRepositoryInterface;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;

/**
 * Class UpdateTaskHandler
 * @package App\Api\V1\Application\Task\UpdateTask
 */
class UpdateTaskHandler implements Handler
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * UpdateTaskHandler constructor.
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param Command|UpdateTask $command
     * @return Task
     * @throws ModelNotSavedException
     */
    public function handle(Command $command)
    {
        $task = $command->getTask();
        $task->name = $command->getName();

        $this->taskRepository->store($task);

        return $task;
    }
}
