<?php

namespace App\Api\V1\Application\Task\UpdateTask;

use App\Domain\Task\Task;
use ItDevgroup\CommandBus\Command;

/**
 * Class UpdateTask
 * @package App\Api\V1\Application\Task\UpdateTask
 */
class UpdateTask implements Command
{
    /**
     * @var string
     */
    private $name;
    /**
    * @var Task
    */
    private $task;

    /**
     * Update Task constructor.
     * @param task $task
     * @param string $name
     */
    public function __construct(
        Task $task,
        string $name
    ) {
        $this->task = $task;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Task
     */
    public function getTask(): Task
    {
        return $this->task;
    }
}
