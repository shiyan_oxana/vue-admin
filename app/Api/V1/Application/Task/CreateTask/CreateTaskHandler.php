<?php

namespace App\Api\V1\Application\Task\CreateTask;

use App\Domain\Core\ModelNotSavedException;
use App\Domain\Task\Task;
use App\Domain\Task\TaskRepositoryInterface;
use ItDevgroup\CommandBus\Command;
use ItDevgroup\CommandBus\Handler;

/**
 * Class CreateTaskHandler
 * @package App\Api\V1\Application\Task\CreateTask
 */
class CreateTaskHandler implements Handler
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * CreateTaskHandler constructor.
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository
    ) {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param Command|CreateTask $command
     * @return Task
     * @throws ModelNotSavedException
     */
    public function handle(Command $command)
    {
        $task = new Task();
        $task->name = $command->getName();

        $this->taskRepository->store($task);

        return $task;
    }
}
