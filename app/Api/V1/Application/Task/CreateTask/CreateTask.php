<?php

namespace App\Api\V1\Application\Task\CreateTask;

use ItDevgroup\CommandBus\Command;

/**
 * Class CreateTask
 * @package App\Api\V1\Application\Task\CreateTask
 */
class CreateTask implements Command
{
    /**
     * @var string
     */
    private $name;

    /**
     * CreateTask constructor.
     * @param string $name
     */
    public function __construct(
        string $name
    ) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
