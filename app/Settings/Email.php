<?php
namespace App\Settings;

use App\Domain\Settings\SettingContainerAbstract;
use App\Domain\Settings\SettingsManagerInterface;

/**
 * Class Email
 * @package App\Settings
 */
class Email extends SettingContainerAbstract
{
    /**
     * @var string
     */
    protected $key = 'email';

    /**
     * @var string
     */
    protected $label = 'Email settings';

    /**
     * @var int
     */
    protected $order = 5;

    /**
     * @return array
     */
    public function emailService()
    {
        return [
            [
                'value' => '',
                'text' => 'Default'
            ],
            [
                'value' => 'sendpulse.com',
                'text' => 'sendpulse.com'
            ]
        ];
    }
}
