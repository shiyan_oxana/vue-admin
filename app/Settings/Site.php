<?php
namespace App\Settings;

use App\Domain\Settings\SettingContainerAbstract;
use App\Domain\Settings\SettingsManagerInterface;

/**
 * Class Site
 * @package App\Settings
 */
class Site extends SettingContainerAbstract
{
    /**
     * @var string
     */
    protected $key = 'site';

    /**
     * @var string
     */
    protected $label = 'Site settings';

    /**
     * @var int
     */
    protected $order = 6;
}
