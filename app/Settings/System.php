<?php
namespace App\Settings;

use App\Domain\Settings\SettingContainerAbstract;
use App\Domain\Settings\SettingsManagerInterface;
use App\Contracts\Payments\PaymentService;

/**
 * Class System
 * @package App\Settings
 */
class System extends SettingContainerAbstract
{
    /**
     * @var string
     */
    protected $key = 'system';

    /**
     * @var string
     */
    protected $label = 'System settings';

    /**
     * @var int
     */
    protected $order = 1;

    /**
     * @param SettingsManagerInterface $manager
     *
     * @return array
     */
    public function containers(SettingsManagerInterface $manager)
    {
        $containers = $manager->all();

        $data = [];
        foreach ($containers as $container) {
            $data[] = [
                'value' => $container->key(),
                'text' => $container->label()
            ];
        }
        return $data;
    }

    /**
     * @return array
     */
    public function storages()
    {
        return [
            'local' => 'local',
            's3' => 's3'
        ];
    }

    /**
     * @return array
     */
    public function boolean()
    {
        return [
            [
                'value' => 0,
                'text' => 'No'
            ],
            [
                'value' => 1,
                'text' => 'Yes'
            ]
        ];
    }
}
