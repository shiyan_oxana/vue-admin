<?php
namespace App\Settings;

use App\Domain\Settings\SettingContainerAbstract;

/**
 * Class Counts
 * @package App\Settings
 */
class Counts extends SettingContainerAbstract
{
    /**
     * @var string
     */
    protected $key = 'counts';

    /**
     * @var string
     */
    protected $label = 'Counts';

    /**
     * @var int
     */
    protected $order = 3;
}
