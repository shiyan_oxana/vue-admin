<?php
namespace App\Infrastructure\Settings\Items;

class Boolean extends SettingItemAbstract
{
    protected $template = 'boolean';
}
