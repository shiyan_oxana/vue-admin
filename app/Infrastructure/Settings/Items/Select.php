<?php
namespace App\Infrastructure\Settings\Items;

/**
 * Class Select
 * @package App\Infrastructure\Settings\Items
 */
class Select extends SettingItemAbstract
{
    /**
     * @var string
     */
    protected $template = 'dashboard.settings.form-items.select';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function render()
    {
        return view($this->template, [
            'label' => $this->label,
            'name' => $this->itemKey,
            'value' => $this->itemValue(),
            'description' => $this->description,
            'mask' => $this->mask,
            'options' => is_array($this->options)
                ? json_encode($this->prepareOptions($this->options))
                : $this->options
        ]);
    }

    /**
     * @param array $options
     * @return array
     */
    protected function prepareOptions($options)
    {
        $data = [];
        foreach ($options as $key => $option) {
            $data[] = [
                'value' => $key,
                'text' => $option,
            ];
        }
        return $data;
    }
}
