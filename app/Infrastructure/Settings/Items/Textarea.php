<?php
namespace App\Infrastructure\Settings\Items;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class Textarea extends SettingItemAbstract
{
    protected $template = 'dashboard.settings.form-items.textarea';

    /**
     * @return Factory|View
     */
    public function render()
    {
        return view($this->template, [
            'label' => $this->label,
            'name' => $this->itemKey,
            'value' => $this->itemValue,
            'description' => $this->description,
        ]);
    }
}
