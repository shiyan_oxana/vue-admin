<?php
namespace App\Infrastructure\Settings\Items;

/**
 * Class Text
 * @package App\Infrastructure\Settings\Items
 */
class Text extends SettingItemAbstract
{
    /**
     * @var string
     */
    protected $template = 'dashboard.settings.form-items.text';

    /**
     * @var string|null
     */
    protected $alias = null;

    /**
     * @var string|null
     */
    protected $placeholder = null;

    /**
     * @var string|null
     */
    protected $separator=null;

    /**
     * Text constructor.
     * @param $key
     * @param $config
     */
    public function __construct($key, $config)
    {
        parent::__construct($key, $config);
        $this->alias = isset($config['alias']) ? $config['alias'] : null;
        $this->placeholder = isset($config['placeholder']) ? $config['placeholder'] : null;
        $this->separator = isset($config['separator']) ? $config['separator'] : null;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function render()
    {
        return view($this->template, [
            'label' => $this->label,
            'name' => $this->itemKey,
            'value' => $this->itemValue,
            'description' => $this->description,
            'mask' => $this->mask,
            'alias' => $this->alias,
            'placeholder' => $this->placeholder,
            'separator' => $this->separator
        ]);
    }
}
