<?php
namespace App\Infrastructure\Settings\Items;

use App\Domain\Settings\SettingItem;
use Illuminate\Support\Facades\Validator;

abstract class SettingItemAbstract implements SettingItem
{
    /**
     * @var string
     */
    protected $template = '';

    /**
     * @var string
     */
    protected $itemKey = '';

    /**
     * @var null
     */
    protected $itemValue = null;

    /**
     * @var null|string
     */
    protected $defaultValue = null;

    /**
     * @var string
     */
    protected $label = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var string
     */
    protected $validationRules = '';

    /**
     * @var null|array
     */
    public $options = null;

    /**
     * @var null|string
     */
    protected $mask = null;

    /**
     * SettingItem constructor.
     * @param string $key
     * @param $config
     */
    public function __construct($key, $config)
    {
        $this->itemKey = $key;
        $this->defaultValue = isset($config['default']) ? $config['default'] : '';
        $this->label = $config['label'];
        $this->description = isset($config['description']) ? $config['description'] : '';
        $this->validationRules = isset($config['validationRules']) ? $config['validationRules'] : '';
        $this->mask = isset($config['mask']) ? $config['mask'] : null;
    }

    /**
     * @return string
     */
    public function template()
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function itemKey()
    {
        return $this->itemKey;
    }

    /**
     * @return null|string
     */
    public function itemValue()
    {
        return is_null($this->itemValue) ? $this->defaultValue : $this->itemValue;
    }

    /**
     * @param $itemValue
     * @throws \Exception
     */
    public function setItemValue($itemValue)
    {
        $validator = Validator::make([
            $this->itemKey => $itemValue
        ], [
            $this->itemKey => $this->validationRules
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            throw new \Exception($errors->first($this->itemKey));
        }
        $this->itemValue = $itemValue;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function description()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function validationRules()
    {
        return $this->validationRules;
    }

    /**
     * @return string
     */
    public function render()
    {
        return '';
    }
}
