<?php
namespace App\Infrastructure\Settings;

use App\Domain\Settings\SettingNotFound;
use App\Domain\Settings\SettingsManagerInterface;
use App\Domain\Settings\SettingsRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;

class SystemSettingsManager implements SettingsManagerInterface
{
    /**
     * @var SettingsRepositoryInterface
     */
    private $repository;

    /**
     * @var array
     */
    private $items;

    /**
     * @var string
     */
    private $namespace = 'App\Settings\\';


    /**
     * SystemSettingsManager constructor.
     * @param SettingsRepositoryInterface $repository
     */
    public function __construct(SettingsRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     */
    public function init()
    {
        if (!$this->items) {
            $this->load();
        }
    }

    /**
     * @param string $key
     * @return mixed
     * @throws SettingNotFound
     */
    public function get($key)
    {
        $this->init();
        $this->checkKey($key);
        $keyParts = explode('.', $key);
        if ($keyParts[1] == '*') {
            return $this->items[$keyParts[0]];
        }
        return $this->items[$keyParts[0]]->get($keyParts[1]);
    }

    /**
     * @param string $key
     * @return bool|string
     */
    public function getByKey($key)
    {
        try {
            $item = $this->repository->byKey($key);

            return $item->value;
        } catch (ModelNotFoundException $ex) {
            return false;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @throws SettingNotFound
     */
    public function set($key, $value)
    {
        $this->init();
        $keyParts = explode('.', $key);
        $this->checkKey($key);
        $item = $this->items[$keyParts[0]];

        $item->set($keyParts[1], $value);
        $item->store();
    }

    /**
     * @return array
     */
    public function all()
    {
        if (!$this->items) {
            $this->init();
        }
        return $this->items;
    }

    /**
     * loads actual settings from configuration files
     */
    private function load()
    {
        $this->initItems();

        $settingsModels = $this->repository->all();

        foreach ($settingsModels as $settingModel) {
            try {
                if (isset($this->items[$settingModel->group])
                    && !is_null($this->items[$settingModel->group]->get($settingModel->key))
                ) {
                    $this->items[$settingModel->group]->set($settingModel->key, $settingModel->value);
                }
            } catch (SettingNotFound $e) {
                $this->repository->delete($settingModel);
            }
        }
        foreach ($this->items as $item) {
            $item->store($this->items);
        }
    }

    /**
     *  instantiate settings from classes
     */
    private function initItems()
    {
        $settingsClasses = File::files(app_path() . DIRECTORY_SEPARATOR . "Settings");

        $items = [];
        foreach ($settingsClasses as $classPath) {
            $parts = explode(DIRECTORY_SEPARATOR, $classPath);
            if (!count($parts)) {
                continue;
            }

            $className = $this->namespace . str_replace('.php', '', end($parts));
            if (!class_exists($className)) {
                include_once $classPath;
            }

            if (!class_exists($className)) {
                continue;
            }

            $object = resolve($className);
            $object->init();

            $items[] = $object;
        }

        $this->sortByOrder($items);
    }

    /**
     * @param string $key
     * @throws SettingNotFound
     */
    protected function checkKey($key)
    {
        $keyParts = explode('.', $key);
        if (count($keyParts) != 2) {
            throw SettingNotFound::fromKey($key);
        }
        if (!isset($this->items[$keyParts[0]])) {
            throw SettingNotFound::fromKey($key);
        }
    }

    /**
     * @param $items
     */
    protected function sortByOrder($items)
    {
        usort($items, function ($a, $b) {
            if ($a->order() == $b->order()) {
                return 0;
            }
            return ($a->order() < $b->order()) ? -1 : 1;
        });
        foreach ($items as $item) {
            $this->items[$item->key()] = $item;
        }
    }
}
