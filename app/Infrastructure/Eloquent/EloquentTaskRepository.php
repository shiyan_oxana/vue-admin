<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\Core\Filter;
use App\Domain\Task\Task;
use App\Domain\Task\TaskFilter;
use App\Domain\Core\Sorting;
use App\Domain\Task\TaskRepositoryInterface;

/**
 * Class EloquentTaskRepository
 * @package App\Infrastructure\Eloquent
 */
class EloquentTaskRepository extends AbstractEloquentRepository implements TaskRepositoryInterface
{
    /**
     * EloquentTaskRepository constructor.
     * @param Task $model
     */
    public function __construct(Task $model)
    {
        $this->model = $model;
    }


    /**
     * @param Filter|TaskFilter $filter
     */
    public function addFilter(Filter $filter): void
    {

    }

    /**
     * @param Sorting $sorting
     */
    public function sort(Sorting $sorting)
    {
    }
}
