<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\Core\Filter;
use App\Domain\Core\ModelNotSavedException;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class AbstractEloquentRepository
 * @package App\Infrastructure\Eloquent
 */
class AbstractEloquentRepository
{
    /**
     * @var Model
     */
    protected $model;
    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @param Filter $filter
     * @param Pagination|null $pagination
     * @param Sorting|null $order
     * @param array|null $relations
     * @return Model[]|LengthAwarePaginator|Collection
     */
    public function all(
        Filter $filter,
        ?Pagination $pagination = null,
        ?Sorting $order = null,
        ?array $relations = null
    ) {
        $this->builder = $this->model->newQuery();
        $this->addFilter($filter);

        if ($order) {
            $this->sort($order);
            $this->builder->orderBy($order->field(), $order->direction());
        }

        if ($relations) {
            $this->addRelations($relations);
        }

        if ($pagination) {
            return $this->builder->paginate(
                $pagination->limit(),
                ['*'],
                'page',
                $pagination->page()
            );
        }

        return $this->builder->get();
    }

    /**
     * @param array $relations
     */
    public function addRelations(array $relations): void
    {
        foreach ($relations as $relation) {
            $this->builder->with($relation);
        }
    }

    /**
     * @param int $id
     * @return Model
     */
    public function byId(int $id): Model
    {
        return $this->model->newQuery()
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * @param Filter $filter
     */
    public function addFilter(Filter $filter): void
    {
    }

    /**
     * @param Model $model
     * @throws ModelNotSavedException
     */
    public function store(Model $model): void
    {
        if (!$model->save()) {
            throw ModelNotSavedException::modelNotSaved();
        }
    }

    /**
     * @param Model $model
     * @return void
     * @throws Exception
     */
    public function delete(Model $model): void
    {
        $model->delete();
    }

    /**
     * @param array $fields
     * @return Model
     * @throws ModelNotFoundException
     */
    public function getFirstByFields(array $fields): Model
    {
        $this->builder = $this->model->newQuery();
        foreach ($fields as $key => $value) {
            $this->builder->where($key, $value);
        }
        return $this->builder->firstOrFail();
    }

    /**
     * @param Builder
     * @param $table
     * @return bool
     */
    protected function isJoined($queryBuilder, $table)
    {
        $joins = collect($queryBuilder->getQuery()->joins);
        return $joins->pluck('table')->contains($table);
    }

    /**
     * @param Model $model
     */
    public function forceDelete(Model $model): void
    {
        $model->forceDelete();
    }

    /**
     * @param Sorting $sorting
     */
    public function sort(Sorting $sorting)
    {
    }
}
