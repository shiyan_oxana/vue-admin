<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\Core\ModelNotSavedException;
use App\Domain\Settings\Setting;
use App\Domain\Settings\SettingsRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class EloquentSettingsRepository
 * @package App\Infrastructure\Eloquent
 */
class EloquentSettingsRepository implements SettingsRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;
    /**
     * @var Builder
     */
    protected $builder;

    /**
     * EloquentSettingsRepository constructor.
     * @param Setting $model
     */
    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->model->newQuery()->get();
    }

    /**
     * @param $key
     * @return Setting|Model
     */
    public function byKey($key)
    {
        return $this->model->newQuery()
            ->where('key', $key)
            ->firstOrFail();
    }

    /**
     * @param int $id
     * @return Model
     */
    public function byId(int $id): Model
    {
        return $this->model->newQuery()
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * @param Model $model
     * @throws ModelNotSavedException
     */
    public function store(Model $model): void
    {
        $this->model = $model;
        if (!$this->model->save()) {
            throw ModelNotSavedException::modelNotSaved();
        }
    }

    /**
     * @param Model $model
     * @return void
     * @throws Exception
     */
    public function delete(Model $model): void
    {
        $this->model = $model;
        $this->model->delete();
    }

    /**
     * @param array $fields
     * @return Model
     * @throws ModelNotFoundException
     */
    public function getFirstByFields(array $fields): Model
    {
        $this->builder = $this->model->newQuery();
        foreach ($fields as $key => $value) {
            $this->builder->where($key, $value);
        }
        return $this->builder->firstOrFail();
    }

    /**
     * @param Builder
     * @param $table
     * @return bool
     */
    protected function isJoined($queryBuilder, $table)
    {
        $joins = collect($queryBuilder->getQuery()->joins);
        return $joins->pluck('table')->contains($table);
    }

    /**
     * @param Model $model
     */
    public function forceDelete(Model $model): void
    {
        $this->model = $model;
        $this->model->forceDelete();
    }
}
