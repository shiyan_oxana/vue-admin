<?php

namespace App\Infrastructure\Eloquent;

use App\Domain\Core\Filter;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use App\Domain\User\UserRepositoryInterface;
use Generator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class EloquentUserRepository
 * @package App\Infrastructure\Eloquent
 */
class EloquentUserRepository extends AbstractEloquentRepository implements UserRepositoryInterface
{
    /**
     * EloquentUserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param UserFilter|Filter $filter
     */
    public function addFilter(Filter $filter): void
    {
        if ($filter->getRoleId()) {
            $this->builder->whereHas(
                'roles',
                function (Builder $q) use ($filter) {
                    $q->where('id', $filter->getRoleId());
                }
            );
        }

        if ($filter->getQuery()) {
            $this->builder->where(
                function (Builder $q) use ($filter) {
                    $q->where('email', 'like', '%' . $filter->getQuery() . '%');
                }
            );
        }
    }

    /**
     * @param Sorting $sorting
     */
    public function sort(Sorting $sorting)
    {
        if (in_array($sorting->field(), ['accounts.full_name'])
            && !$this->isJoined($this->builder, 'accounts')
        ) {
            $this->builder->leftJoin(
                'accounts',
                'users.id',
                '=',
                'accounts.user_id'
            );
            $this->builder->select('accounts.*');
        }
    }

    /**
     * @param string $email
     * @return Model
     */
    public function byEmail($email): Model
    {
        $this->builder = $this->model->newQuery();
        $this->builder->whereRaw('LOWER(email) = \'' . Str::lower($email) . '\'');

        return $this->builder->firstOrFail();
    }

    /**
     * @param string $apiKey
     * @return Model
     */
    public function byApiKey(string $apiKey): Model
    {
        $this->builder = $this->model->newQuery();
        $this->builder->whereHas('account', function ($q) use ($apiKey) {
            $q->where('api_key', $apiKey);
        });

        return $this->builder->firstOrFail();
    }
}
