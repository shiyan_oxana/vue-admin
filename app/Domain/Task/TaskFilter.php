<?php

namespace App\Domain\Task;

use Illuminate\Http\Request;
use App\Domain\Core\Filter;

/**
 * Class TaskFilter
 * @package App\Domain\Task
 */
class TaskFilter implements Filter
{
    /**
     * @param Request $request
     * @return TaskFilter
     */
    public static function fromRequest(Request $request): TaskFilter
    {
        return (new TaskFilter());
    }
}
