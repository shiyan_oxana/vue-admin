<?php

namespace App\Domain\Task;

use Exception;

/**
 * Class TaskException
 * @package App\Domain\Task
 */
class TaskException extends Exception
{
    /**
     * @return TaskException
     */
    public static function taskNotSaved()
    {
        return new TaskException('Task cannot be saved');
    }
}
