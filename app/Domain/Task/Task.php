<?php

namespace App\Domain\Task;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App\Domain\Task
 */
class Task extends Model
{
    /**
     * @type array
     */
    const ALLOWED_SORT_FIELDS = [];

    /**
     * @var string
     */
    protected $table = 'tasks';
}
