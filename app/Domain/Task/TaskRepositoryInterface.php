<?php

namespace App\Domain\Task;

use App\Domain\Core\ModelNotSavedException;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface TaskRepositoryInterface
 * @package App\Domain\Task
 */
interface TaskRepositoryInterface
{
    /**
     * @param int $id
     * @return Task
     */
    public function byId(int $id): Model;

    /**
     * @param Model $model
     * @throws ModelNotSavedException
     */
    public function store(Model $model): void;

    /**
     * @param Model $model
     * @return void
     * @throws Exception
     */
    public function delete(Model $model): void;

    /**
     * @param TaskFilter $filter
     * @param Pagination|null $pagination
     * @param Sorting|null $order
     * @return mixed
     */
    public function all(TaskFilter $filter, ?Pagination $pagination = null, ?Sorting $order = null);

    /**
     *
     * @param array $fields
     * @return Task|Model|null
     */
    public function getFirstByFields(array $fields): Model;
}
