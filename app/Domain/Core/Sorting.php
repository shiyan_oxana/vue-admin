<?php
namespace App\Domain\Core;

use Illuminate\Http\Request;

class Sorting
{
    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';
    const SORT_DIRS = [
        self::SORT_ASC,
        self::SORT_DESC,
    ];
    const SORT_FIELD = 'created_at';

    /**
     * @var string
     */
    private $field = 'created_at';

    /**
     * @var string
     */
    private $direction = 'desc';


    /**
     * @return string
     */
    public function field()
    {
        return $this->field;
    }

    /**
     * @param string $field
     *
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return string
     */
    public function direction()
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     *
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @param Request $request
     * @param string[] $allowedFields
     * @param string $defaultSortField
     * @param string $direction
     * @param string $sortParam
     * @return $this
     */
    public static function fromRequest(
        Request $request,
        array $allowedFields,
        $defaultSortField = self::SORT_FIELD,
        $direction = self::SORT_ASC,
        $sortParam = 'sort'
    ) {
        $param = $request->get($sortParam);

        if (substr($param, 0, 1) == '-') {
            $direction = self::SORT_DESC;
            $param = ltrim($param, '-');
        }

        $sortField = !in_array($param, array_keys($allowedFields))
            ? $allowedFields[$defaultSortField] ?? 'created_at'
            : $allowedFields[$param];

        return (new self())
            ->setField($sortField)
            ->setDirection($direction);
    }
}
