<?php

namespace App\Domain\Core;

use Throwable;

/**
 * Class EntityNotFound
 * @package App\Domain\Core
 */
abstract class EntityNotFound extends \Exception
{
    /**
     * EntityNotFound constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }
}
