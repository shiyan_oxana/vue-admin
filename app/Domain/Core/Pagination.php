<?php

namespace App\Domain\Core;

use App\Infrastructure\Settings\SystemSettingsManager;
use Illuminate\Http\Request;

/**
 * Class Pagination
 * @package App\Domain\Core
 */
class Pagination
{
    /**
     * Default page and per page values.
     */
    const DEFAULT_PAGE = 1;
    const DEFAULT_PER_PAGE = 1;
    const ON_EACH_SIDE = 2;

    const PER_PAGE_LIST = [
        5 => 5,
        10 => 10,
        20 => 20,
        50 => 50,
        100 => 100,
    ];

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage;

    /**
     * Pagination constructor.
     *
     * @param int $page
     * @param int $perPage
     * @throws \App\Domain\Settings\SettingNotFound
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct($page = self::DEFAULT_PAGE, $perPage = null)
    {
        $this->page = $page;
        $this->perPage = $perPage ?: self::getDefaultPerPage();
    }

    /**
     * @return int
     */
    public function page()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function offset()
    {
        return $this->perPage * ($this->page - 1);
    }

    /**
     * @return int
     */
    public function limit()
    {
        return $this->perPage;
    }

    /**
     * @param Request $request
     * @return Pagination
     * @throws \App\Domain\Settings\SettingNotFound
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function fromRequest(Request $request)
    {
        return new Pagination(self::getPageNumberFromRequest($request), self::getPerPageFromRequest($request));
    }

    /**
     * @param Request $request
     * @return int|mixed
     */
    public static function getPageNumberFromRequest(Request $request)
    {
        return $request->get('page', self::DEFAULT_PAGE);
    }

    /**
     * @param Request $request
     * @return int|mixed
     * @throws \App\Domain\Settings\SettingNotFound
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function getPerPageFromRequest(Request $request)
    {
        return abs($request->get('perPage', self::getDefaultPerPage()));
    }

    /**
     * @return int|mixed
     * @throws \App\Domain\Settings\SettingNotFound
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function getDefaultPerPage()
    {
        /** @var SystemSettingsManager $settings */
        $settings = app()->make(SystemSettingsManager::class);
        if (!\request()) {
            return  $settings->get('counts.rows_count') ?? self::DEFAULT_PER_PAGE;
        }

        if (\request()->route()->gatherMiddleware()[0] == 'web') {
            return   $settings->get('counts.rows_count') ?? self::DEFAULT_PER_PAGE;
        }
        return $settings->get('counts.ui_rows_count_default') ?? self::DEFAULT_PER_PAGE;
    }
}
