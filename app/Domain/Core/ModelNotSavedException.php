<?php

namespace App\Domain\Core;

use Exception;

/**
 * Class ModelNotSavedException
 * @package App\Domain\Core
 */
class ModelNotSavedException extends Exception
{
    /**
     * @return ModelNotSavedException
     */
    public static function modelNotSaved()
    {
        return new ModelNotSavedException('Model not saved');
    }
}
