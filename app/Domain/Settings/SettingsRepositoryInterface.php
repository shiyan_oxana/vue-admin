<?php
namespace App\Domain\Settings;

use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use App\Domain\EmailTemplate\EmailTemplateFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface SettingsRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all();

    /**
     * @param string $key
     * @return Setting
     */
    public function byKey($key);

    /**
     * @param Model $model
     * @return void
     */
    public function store(Model $model);

    /**
     * @param Model $model
     * @return void
     */
    public function delete(Model $model);
}
