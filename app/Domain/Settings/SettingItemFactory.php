<?php
namespace App\Domain\Settings;

class SettingItemFactory
{
    private static $namespace = 'App\Infrastructure\Settings\Items\\';

    private static $default = 'text';

    public static function create($key, $itemData)
    {
        $type = isset($itemData['type']) ? $itemData['type'] : self::$default;
        $className = self::$namespace . ucfirst($type);
        return new $className($key, $itemData);
    }
}
