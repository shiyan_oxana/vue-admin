<?php
namespace App\Domain\Settings;

use Exception;

class SettingNotFound extends Exception
{
    /**
     * @param $key
     *
     * @return SettingNotFound
     */
    public static function fromKey($key)
    {
        return new SettingNotFound("Setting with key #{$key} not found.");
    }
}
