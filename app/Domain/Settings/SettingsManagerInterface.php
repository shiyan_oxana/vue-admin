<?php
namespace App\Domain\Settings;

interface SettingsManagerInterface
{
    /**
     * get value from properties
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * get value from database
     * @param string $key
     * @return bool|string
     */
    public function getByKey($key);

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value);

    /**
     * @return array
     */
    public function all();
}
