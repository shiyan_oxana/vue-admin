<?php
namespace App\Domain\Settings;

interface SettingContainer
{
    /**
     * @return array
     */
    public function all();

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value);
}
