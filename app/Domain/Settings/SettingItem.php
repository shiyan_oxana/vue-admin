<?php
namespace App\Domain\Settings;

/**
 * Interface SettingItem
 * @package App\Domain\Settings
 */
interface SettingItem
{
    /**
     * @return string
     */
    public function template();

    /**
     * @return string
     */
    public function itemKey();

    /**
     * @return mixed
     */
    public function itemValue();

    /**
     * @param $itemValue
     * @return void
     */
    public function setItemValue($itemValue);

    /**
     * @param $data
     * @return void
     */
    public function setOptions($data);

    /**
     * @return mixed
     */
    public function render();
}
