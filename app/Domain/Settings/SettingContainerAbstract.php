<?php
namespace App\Domain\Settings;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\Yaml\Yaml;

abstract class SettingContainerAbstract implements SettingContainer
{
    /**
     * @var string
     */
    protected $key = '';

    /**
     * @var string
     */
    protected $label = '';

    /**
     * @var int
     */
    protected $order = 1;

    /**
     * @var string
     */
    protected $fieldsFile = 'fields.yaml';

    /**
     * @var array
     */
    protected $yamlFields = [];

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var array
     */
    protected $modelValues = [];

    /**
     * @var Setting
     */
    protected $model = null;

    /**
     * @var SettingsRepositoryInterface
     */
    protected $settingsRepository;

    /**
     * SettingItem constructor.
     * @param SettingsRepositoryInterface $settingsRepository
     */
    public function __construct(SettingsRepositoryInterface $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * init settings items
     */
    public function init()
    {
        $folder = strtolower(class_basename(static::class));
        $this->yamlFields = Yaml::parse(file_get_contents(
            app_path()
            . '/Settings/'
            . $folder
            . '/' . $this->fieldsFile
        ));

        foreach ($this->yamlFields as $key => $item) {
            $this->items[$key] = SettingItemFactory::create($key, $item);
            if (isset($item['options'])) {
                $this->items[$key]->setOptions($item['options']);
            }
        }
    }

    protected function prepareItems()
    {
        foreach ($this->yamlFields as $key => $item) {
            $this->items[$key] = isset($item['default']) ? $item['default'] : '';
        }
    }

    /**
     * @param null $items
     */
    public function store($items = null)
    {
        /** @var SettingItem $item */
        foreach ($this->items as $key => $item) {
            try {
                if ($items && array_key_exists($this->key, $items)) {
                    continue;
                }
                $setting = $this->settingsRepository->byKey($item->itemKey());
            } catch (ModelNotFoundException $e) {
                $setting = new Setting();
                $setting->key = $item->itemKey();
                $setting->group = $this->key;
            }
            $setting->value = $item->itemValue();
            $this->settingsRepository->store($setting);
        }
    }

    public function key()
    {
        return $this->key;
    }

    public function all()
    {
        return $this->items;
    }

    public function set($key, $value)
    {
        if (!isset($this->items[$key])) {
            throw SettingNotFound::fromKey($key);
        }

        $this->items[$key]->setItemValue($value);
    }

    public function get($key)
    {
        if (!isset($this->items[$key])) {
            throw SettingNotFound::fromKey($key);
        }
        return is_object($this->items[$key]) ? $this->items[$key]->itemValue() : $this->items[$key];
    }

    public function label()
    {
        return $this->label;
    }

    public function order()
    {
        return $this->order;
    }

    /**
     * @return array
     */
    public function booleans()
    {
        return [
            true => 'Yes',
            false => 'No'
        ];
    }
}
