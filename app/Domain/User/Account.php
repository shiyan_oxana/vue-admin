<?php

namespace App\Domain\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Domain\User\Account
 *
 * @property int $id
 * @property int $user_id
 * @property string $full_name
 * @property string|null $phone
 * @property string|null $address
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $is_partner
 * @property int|null $referral_link_id
 * @property string|null $smart_uid
 * @property string|null $api_key
 * @property-read User $user
 * @method static Builder|Account whereApiKey($value)
 * @method static Builder|Account newModelQuery()
 * @method static Builder|Account newQuery()
 * @method static Builder|Account query()
 * @method static Builder|Account whereAddress($value)
 * @method static Builder|Account whereCreatedAt($value)
 * @method static Builder|Account whereFullName($value)
 * @method static Builder|Account whereId($value)
 * @method static Builder|Account wherePhone($value)
 * @method static Builder|Account whereUpdatedAt($value)
 * @method static Builder|Account whereUserId($value)
 * @method static Builder|Account whereIsPartner($value)
 * @method static Builder|Account whereReferralLinkId($value)
 * @method static Builder|Account whereSmartUid($value)
 * @mixin Eloquent
 */
class Account extends Model
{
    /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone'
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
