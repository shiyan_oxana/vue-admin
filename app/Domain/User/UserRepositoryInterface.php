<?php

namespace App\Domain\User;

use App\Domain\Core\Filter;
use App\Domain\Core\ModelNotSavedException;
use App\Domain\Core\Pagination;
use App\Domain\Core\Sorting;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface UserRepositoryInterface
 * @package App\Domain\User
 */
interface UserRepositoryInterface
{
    /**
     * @param int $id
     * @return User
     */
    public function byId(int $id): Model;

    /**
     * @param Model $model
     * @throws ModelNotSavedException
     */
    public function store(Model $model): void;

    /**
     * @param Model $model
     * @return void
     * @throws \Exception
     */
    public function delete(Model $model): void;

    /**
     * @param Filter $filter
     * @param Pagination|null $pagination
     * @param Sorting|null $order
     * @param array|null $relations
     * @return mixed
     */
    public function all(
        Filter $filter,
        ?Pagination $pagination = null,
        ?Sorting $order = null,
        ?array $relations = null
    );

    /**
     *
     * @param array $fields
     * @return User|Model|null
     */
    public function getFirstByFields(array $fields): Model;

    /**
     * @param string $email
     * @return Model
     */
    public function byEmail(string $email): Model;

    /**
     * @param string $apiKey
     * @return Model
     */
    public function byApiKey(string $apiKey): Model;
}
