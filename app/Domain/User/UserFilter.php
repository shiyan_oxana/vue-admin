<?php
namespace App\Domain\User;

use App\Domain\Core\Filter;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class UserFilter
 * @package App\Domain\User
 */
class UserFilter implements Filter
{
    /**
     * @var int|null
     */
    private ?int $roleId = null;
    /**
     * @var string|null
     */
    private ?string $query = null;

    /**
     * @param Request $request
     * @return UserFilter
     */
    public static function fromRequest(Request $request): UserFilter
    {
        return (new UserFilter())
            ->setQuery($request->get('query'))
            ->setRoleId($request->get('roleId'));
    }
    /**
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @param string|null $query
     * @return UserFilter
     */
    public function setQuery(?string $query): UserFilter
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoleId(): ?int
    {
        return $this->roleId;
    }

    /**
     * @param int|null $roleId
     * @return UserFilter
     */
    public function setRoleId(?int $roleId): UserFilter
    {
        $this->roleId = $roleId;

        return $this;
    }
}
