<?php

return [
    'namespace' => 'app',
    'paths' => [
        'domain' => 'Domain',
        'command' => 'Api/V1/Application',
        'repository' => 'Infrastructure/Eloquent',
        'controller' => 'Api/V1/Http/Controllers',
        'request' => 'Api/V1/Http/Requests',
        'resource' => 'Api/V1/Http/Resources',
        'migrate' => 'database/migrations',
        'test' => 'tests/Feature',
        'doc' => 'api-doc',
        'stub' => null,
        'route' => base_path('routes/api.php'),
    ],
    'repositoryFilePrefix' => 'Eloquent',
    'generate' => [
        'collection' => true
    ],
    'declare' => true
];
