## Set up
2. Make .env `cp .env.example .env`
3. Run docker `docker-compose up -d`
4. Install vendors `docker-compose exec php-fpm composer install`
5. Generate app key `docker-compose exec php-fpm php artisan key:generate`
6. Run migration `docker-compose exec php-fpm php artisan migrate --seed`
7. Run `docker-compose exec php-fpm php artisan passport:install`.
