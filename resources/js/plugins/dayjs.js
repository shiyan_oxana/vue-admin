import Vue from 'vue'
import dayjs from 'dayjs'
import dayjsLocale from 'dayjs/locale/ru'

Object.defineProperties(Vue.prototype, {
    $date: {
        get() {
            dayjs.locale(dayjsLocale)
            return dayjs
        },
    },
})
