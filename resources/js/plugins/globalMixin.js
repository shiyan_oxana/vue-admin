const globalDateTimeDBFormat = 'YYYY-MM-DD HH:mm:ss'
const globalDateFormat = 'DD/MM/YYYY'
const globalDateTimeFormat = 'DD/MM/YYYY HH:mm'
export default {
    data: () => ({
        globalDateFormat: globalDateFormat,
        globalDateTimeFormat: globalDateTimeFormat,
        globalDateTimeDBFormat: globalDateTimeDBFormat,
        globalPerPage: 10,
    }),
    filters: {
        capitalize: function (value) {
            if (!value) return ''
            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        },
    },
    methods: {
        GlobalIsEmpty(obj, key = '') {
            if (
                typeof key === 'string' &&
                key.length &&
                Object.keys(obj).length
            ) {
                return !Object.prototype.hasOwnProperty.call(obj, key)
            }
            return Object.keys(obj).length === 0 && obj.constructor === Object
        },
    },
}
