import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'

Vue.use(VueAuth, {
    auth: {
        request: function (req, token) {
            if (req.url === this.options.refreshData.url) {
                req.data = {
                    refresh_token: this.token(this.options.refreshTokenName),
                }
            }

            this.http.setHeaders.call(this, req, {
                Authorization: 'Bearer ' + token,
            })
        },
        response: function (res) {
            if (
                res &&
                res.data &&
                res.data.data &&
                res.data.data.type == 'token' &&
                res.data.data.attributes
            ) {
                const resData = res.data.data.attributes || {}
                this.token(this.options.refreshTokenName, resData.refreshToken)
                this.token(this.options.tokenDefaultKey, resData.accessToken)
            }
        },
    },
    refreshTokenName: 'refresh_token',
    // eslint-disable-next-line no-undef
    http: require('@websanova/vue-auth/dist/drivers/http/axios.1.x.js'),
    // eslint-disable-next-line no-undef
    router: require('@websanova/vue-auth/dist/drivers/router/vue-router.2.x.js'),
    loginData: {
        // eslint-disable-next-line no-undef
        url: process.env.MIX_API_ENDPOINT + 'auth/login',
        redirect: '/admin/dashboard',
    },
    logoutData: {
        // eslint-disable-next-line no-undef
        url: process.env.MIX_API_ENDPOINT + 'auth/logout',
        redirect: '/',
        makeRequest: true,
    },
    registerData: {
        // eslint-disable-next-line no-undef
        url: process.env.MIX_API_ENDPOINT + 'auth/register',
        method: 'POST',
        redirect: '/',
    },
    // eslint-disable-next-line no-undef
    fetchData: { url: process.env.MIX_API_ENDPOINT + 'user/me', method: 'GET' },
    refreshData: {
        // eslint-disable-next-line no-undef
        url: process.env.MIX_API_ENDPOINT + 'auth/refresh',
        method: 'POST',
        enabled: false,
        interval: 30,
    },
})
