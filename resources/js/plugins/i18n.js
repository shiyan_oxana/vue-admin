import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from '../lang'
import axios from 'axios'

Vue.use(VueI18n)

const DEFAULT_LANGUAGE = 'en'

const i18n = new VueI18n({
    locale: DEFAULT_LANGUAGE,
    messages,
    silentTranslationWarn: true,
})

setI18nLanguage(DEFAULT_LANGUAGE, i18n)

function setI18nLanguage(lang, i18n) {
    i18n.locale = lang
    axios.defaults.headers.common['Accept-Language'] = lang
    // eslint-disable-next-line no-undef
    document.querySelector('html').setAttribute('lang', lang)
    return lang
}

export default i18n
