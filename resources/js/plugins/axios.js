import Vue from 'vue'
import VueAxios from 'vue-axios'
import axios from 'axios'
import { Message } from 'element-ui'
import store from '../core/store'
import ValidationError from '../modules/validationError/ValidationError'

Vue.use(VueAxios, axios)

// eslint-disable-next-line no-undef
let token = document.head.querySelector('meta[name="csrf-token"]')
axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// eslint-disable-next-line no-undef
axios.defaults.baseURL = process.env.MIX_API_ENDPOINT

axios.interceptors.request.use(async (config) => {
    await store.dispatch('clearErrors')
    return config
})

axios.interceptors.response.use(
    (response) => response,
    (error) => {
        if (!error.response || !error.response.data) {
            // eslint-disable-next-line no-undef
            return Promise.reject(error)
        }

        if (error.response.status >= 500) {
            if (error.response.data.message) {
                Message.error(error.response.data.message)
            } else {
                // eslint-disable-next-line no-undef
                Message.error(window.Vue.$t('global.unknown_server_error'))
            }
        }

        store.dispatch('setErrors', new ValidationError(error.response))

        // eslint-disable-next-line no-undef
        return Promise.reject(error)
    }
)
