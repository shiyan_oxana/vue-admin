import Vue from 'vue'
import App from './core/App'
import ElementUI from 'element-ui'
import i18n from './plugins/i18n'
import router from './plugins/router'
import store from './core/store'
import globalMixin from './plugins/globalMixin'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import vSelect from 'vue-select'
import './plugins/axios'
import './plugins/auth'
import './plugins/dayjs'
import './plugins/vue-js-notification'
import { mapGetters } from 'vuex'

try {
    window.Popper = require('popper.js').default
    window.$ = window.jQuery = require('jquery')
    require('bootstrap')
    window._ = require('lodash')
} catch (e) {
    console.log('Error load main libraries')
}

Vue.use(ElementUI, { i18n: (key, value) => i18n.t(key, value) })
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('v-select', vSelect)

Vue.prototype.config = window.config

Vue.mixin(globalMixin)

Vue.mixin({
    computed: {
        ...mapGetters({
            errors: 'errors',
        }),
    },
})

window.Vue = new Vue({
    router,
    store,
    i18n,
    render: (h) => h(App),
}).$mount('#app')
