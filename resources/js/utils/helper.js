export default class Helper {
    static isEmpty(obj, key = '') {
        if (typeof key === 'string' && key.length && Object.keys(obj).length) {
            return !Object.prototype.hasOwnProperty.call(obj, key)
        }

        return Object.keys(obj).length === 0 && obj.constructor === Object
    }
}
