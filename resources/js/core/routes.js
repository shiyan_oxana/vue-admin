import NotFound     from "./components/NotFound";
import Welcome      from "./components/Welcome";
import Home         from "./components/Home";
import auth         from "../modules/auth/routes_auth";
import Dashboard    from "../modules/dashboard/components/Dashboard";
import Settings     from "../modules/setting/components/SettingsList";

// Load modules routes dynamically.
const requireContext = require.context('../modules', true, /routes\.js$/)
const page = path => () => import(/* webpackChunkName: '' */ `./components/${path}`).then(m => m.default || m)

const modules = requireContext.keys()
    .map(file =>
        [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)]
    )

let moduleRoutes = [];


for(let i in modules) {
    moduleRoutes = moduleRoutes.concat(modules[i][1].routes)
}

export const routes = [
    {
        path: '/admin',
        component: Home,
        meta: {auth: true},
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: Dashboard,
                iconCls: 'el-icon-menu'
            },
            ...moduleRoutes,
            {
                path: 'setting',
                name: 'setting',
                component: Settings,
                iconCls: 'el-icon-setting',
            },
        ]
    },
    {
        path: '/',
        component: Welcome,
        children: [
            ...auth,
            {
                path: '*',
                component: NotFound,
                name: 'not_found'
            }
        ]
    },
];

