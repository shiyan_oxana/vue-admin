let collapsed = false

if (window.innerWidth < 768) {
    collapsed = true;
}

export const store = {
    state: {
        isCollapsed: collapsed,
        errors: null
    },

    mutations: {
        TOGGLE_COLLAPSE(state) {
            state.isCollapsed = !state.isCollapsed
        },
        SET_VALIDATION_ERRORS(state, errors) {
            state.errors = errors
        },
    },
    getters: {
        coreIsCollapsed: state => state.isCollapsed,
        errors: state => state.errors,
    },

    actions: {
        setErrors({ commit }, errors) {
            commit('SET_VALIDATION_ERRORS', errors)
        },
        clearErrors({ commit }) {
            commit('SET_VALIDATION_ERRORS', {})
        },
    }
}
