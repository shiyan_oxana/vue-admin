/**
 * AutoImporting components
 * @param path
 * @returns {function(): *}
 */
const page = path => () => import(/* webpackChunkName: '' */ `./components/${path}`).then(m => m.default || m)

export const routes = [
    {
        path: 'logs',
        name: 'log',
        component: page('LogList'),
        hidden: true
    },
    {
        path: 'logs/:key',
        name: 'show-log',
        component: page('LogView'),
        hidden: true
    }
]
