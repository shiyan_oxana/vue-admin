import H from '../../../utils/helper'

export default class Log {
    constructor(obj = {}) {
        if (obj instanceof Log) {
            Object.keys(obj).forEach((element) => {
                this[element] = obj[element]
            })
        } else {
            this.id = !H.isEmpty(obj) && obj.id ? obj.id : null
            const attributes = ['files']
            const dataAttribute = {}
            attributes.forEach(function (field) {
                dataAttribute[field] =
                    !H.isEmpty(obj) && !H.isEmpty(obj.attributes, field)
                        ? obj.attributes[field]
                        : ''
            })

            Object.assign(this, dataAttribute)
        }
    }
}
