import axios from 'axios'

const API_ENDPOINT = 'log'

export default {
    all(data) {
        return axios.get(API_ENDPOINT, { params: data })
    },

    download(key) {
        return axios.get(`${API_ENDPOINT}/download/${key}`, {
            responseType: 'blob',
        })
    },

    clear(key) {
        return axios.post(`${API_ENDPOINT}/clear/${key}`)
    },
}
