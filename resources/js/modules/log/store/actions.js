import logApi from '../api'
import Log from '../model/Log'

export const actions = {
    async fetchLogs({ commit }, params = null) {
        try {
            const { data } = await logApi.all(params)
            return { data: data.files }
        } catch (e) {
            throw e
        }
    },
    async downloadLogs({ commit }, key) {
        try {
            const response = await logApi.download(key)
            return response
        } catch (e) {
            throw e
        }
    },
    async clearLog({ commit }, key) {
        try {
            const { data } = await logApi.clear(key)
            return { data: data }
        } catch (e) {
            throw e
        }
    },
}
