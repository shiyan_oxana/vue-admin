import { actions } from './actions'

export const store = {
    state: {
        users: [],
    },

    getters: {
        users: (state) =>
            state.users.length
                ? state.users.map((e) => ({ value: e.id, text: e.email }))
                : state.users,
    },

    mutations: {
        SET_USER_LIST(state, users) {
            state.users = users
        },
    },
    actions,
}
