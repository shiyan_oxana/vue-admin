import userApi from '../api'
import User from '../model/User'

export const actions = {
    // eslint-disable-next-line no-unused-vars
    async fetchUsersListAll({ commit }, params = null) {
        try {
            const { data } = await userApi.all(params)
            const users = data.data.map((o) => new User(o))

            return { data: users, meta: data.meta }
        } catch (e) {
            throw e
        }
    },

    async fetchUsersOptions({ commit: commit }, formData = {}) {
        const params = Object.assign(
            {},
            { page: 1, perPage: 100, sort: 'email', roleId: 2 },
            formData
        )
        try {
            const { data } = await userApi.all(params)
            const users = data.data.map((o) => new User(o))

            commit('SET_USER_LIST', users)

            return users
        } catch (e) {}
    },

    async fetchUserById({ commit }, id) {
        try {
            const { data } = await userApi.find(id)
            const user = new User(data.data)

            return user
        } catch (e) {
            throw e
        }
    },

    async deleteUser({ commit }, id) {
        try {
            await userApi.delete(id)
        } catch (e) {
            throw e
        }
    },
}
