import auth from '../auth/routes_auth'
import NotFound from '../../core/components/NotFound'

/**
 * AutoImporting components
 * @param path
 * @returns {function(): *}
 */
const page = (path) => () =>
    import(/* webpackChunkName: '' */ `./components/${path}`).then(
        (m) => m.default || m
    )

export const routes = [
    {
        path: 'users',
        name: 'users',
        component: page('UserList'),
        iconCls: 'el-icon-user-solid',
        children: [
            {
                path: '',
                name: 'users',
                component: page('UserList'),
            },
            /*{
                path: 'list',
                name: 'users-list',
                component: page('UserList'),
            }*/
        ],
    },
    {
        path: 'users/:id',
        name: 'showUser',
        component: page('UserView'),
        hidden: true,
    },
]
