import H from '../../../utils/helper'

export default class User {
    constructor(obj = {}) {
        const attributes = [
            'email',
            'name',
            'apiKey',
            'role',
            'emailVerified',
            'emailVerificationToken',
            'createdAt',
            'updatedAt',
        ]
        const dataAttribute = {}

        this.id = !H.isEmpty(obj) && obj.id ? obj.id : null

        attributes.forEach(function (field) {
            dataAttribute[field] =
                !H.isEmpty(obj) && !H.isEmpty(obj.attributes, field)
                    ? obj.attributes[field]
                    : ''
        })

        Object.assign(this, dataAttribute)
    }
}
