import axios from 'axios'

const API_ENDPOINT = 'settings'

export default {
    all(data) {
        return axios.get(API_ENDPOINT, { params: data })
    },

    update(model) {
        return axios.post(`${API_ENDPOINT}/update`, model)
    },
}
