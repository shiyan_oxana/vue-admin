import settingsApi from '../api'

export const actions = {
    async fetchSettingsList({ commit }, params = null) {
        try {
            const { data } = await settingsApi.all(params)

            return { data: data.data }
        } catch (e) {
            throw e
        }
    },

    async updateSettings({ commit }, model) {
        try {
            const { data } = await settingsApi.update(model)

            return data
        } catch (e) {
            throw e
        }
    },
}
