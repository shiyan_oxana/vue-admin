/**
 * AutoImporting components
 * @param path
 * @returns {function(): *}
 */
const page = (path) => () =>
    import(/* webpackChunkName: '' */ `./components/${path}`).then(
        (m) => m.default || m
    )

export const routes = [
    {
        path: 'tasks',
        name: 'tasks',
        component: page('TaskList'),
    },
    {
        path: 'tasks/:id',
        name: 'Show Task',
        component: page('TaskView'),
        hidden: true,
    },
]
