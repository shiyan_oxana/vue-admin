import taskApi from '../api'
import Task from '../model/Task'

export const actions = {
    // eslint-disable-next-line no-unused-vars
    async fetchTasksList({ commit }, params = null) {
        try {
            const { data } = await taskApi.all(params)
            const tasks = data.data.map((o) => new Task(o))
            return { data: tasks, meta: data.meta }
        } catch (e) {
            throw e
        }
    },

    // eslint-disable-next-line no-unused-vars
    async deleteTask({ commit }, id) {
        try {
            await taskApi.delete(id)
        } catch (e) {
            throw e
        }
    },

    // eslint-disable-next-line no-unused-vars
    async fetchByIdTask({ commit }, id) {
        try {
            const { data } = await taskApi.find(id)
            return new Task(data.data)
        } catch (e) {
            throw e
        }
    },

    // eslint-disable-next-line no-unused-vars
    async updateTask({ commit }, model) {
        try {
            const { data } = await taskApi.update(model)
            return new Task(data.data)
        } catch (e) {
            throw e
        }
    },

    // eslint-disable-next-line no-unused-vars
    async createTask({ commit }, model) {
        try {
            const { data } = await taskApi.create(model)
            return new Task(data.data)
        } catch (e) {
            throw e
        }
    },
}
