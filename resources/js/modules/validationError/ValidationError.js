import H from '../../utils/helper'

export default class ValidationError {
    constructor(obj = {}) {
        this.message =
            !H.isEmpty(obj) && !H.isEmpty(obj.data, 'message')
                ? obj.data.message
                : !H.isEmpty(obj, 'message')
                ? obj.message
                : ''
        this.status =
            !H.isEmpty(obj) && !H.isEmpty(obj, 'status') ? obj.status : ''
        this.fields =
            !H.isEmpty(obj) && !H.isEmpty(obj.data, 'errors')
                ? obj.data.errors
                : ''
    }
}
